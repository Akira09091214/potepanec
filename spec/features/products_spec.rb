require 'rails_helper'

RSpec.feature "Products", type: :feature do
  describe "Go to the require page" do
    let(:product) { create(:product, name: 'Book', price: '28.11', description: 'example text') }

    it "showの情報が正しく表示されること" do
      visit potepan_product_path(product.id)
      expect(page).to have_selector 'h2', text: product.name
      expect(page).to have_selector 'h3', text: product.price
      expect(page).to have_selector 'p', text: product.description
      expect(page).to have_link 'Home'
      expect(page).to have_content '関連商品'
    end

    it "正しいタイトルが表示されること" do
      visit potepan_product_path(product.id)
      expect(page).to have_title 'Book - BIGBAG Store'
    end
  end
end
