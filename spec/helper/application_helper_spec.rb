require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'full title helper' do
    let(:base_title) { 'BIGBAG Store' }

    describe 'page_titleが空の場合' do
      it 'タイトルがBIGBAG Storeとなること' do
        expect(full_title('')).to eq "#{base_title}"
      end
    end

    describe 'page_titleが存在する場合' do
      it 'タイトルがsample - BIGBAG Storeとなること' do
        expect(full_title('example')).to eq "example - #{base_title}"
      end
    end
  end
end
