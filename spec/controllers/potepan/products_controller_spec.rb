require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let(:product) { create :product }

    before do
      get :show, params: { id: product.id }
    end

    it "リクエストが成功すること" do
      expect(response.status).to eq 200
    end

    it "showテンプレートが表示されること" do
      expect(response).to render_template :show
    end

    it "@productが取得できていること" do
      expect(assigns(:product)).to eq product
    end
  end
end
